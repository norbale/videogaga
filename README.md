You can find a running version of the game here : https://norbale.gitlab.io/videogaga/

add git hooks to your hooks :
https://github.com/doitian/unity-git-hooks

What is "nuget packages" ?

Open source game : 
 - https://github.com/ppy/osu
 - https://osgameclones.com/
 - https://github.com/finol-digital/Card-Game-Simulator

 Resolve some problems :
 - https://stackoverflow.com/questions/52189426/how-to-get-intellisense-in-visual-studio-code-for-unity-functions-names

Don't forget to install : 
 - https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-7.0.102-windows-x64-installer
 - https://dotnet.microsoft.com/download/dotnet-framework/net471


Ressources : 

 - https://code.visualstudio.com/docs/other/unity
 - https://forum.unity.com/threads/-cant-get-vscode-to-work-properly-with-unity.538224/
 - https://dotnet.microsoft.com/en-us/download/dotnet-framework/net471
 - https://www.codemaid.net/
 - https://dev.to/srmagura/c-linting-and-formatting-tools-in-2021-bna
 - https://github.com/topfreegames/unity-template/blob/master/.editorconfig
 - https://github.com/DotNetAnalyzers/StyleCopAnalyzers


