using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingScript : MonoBehaviour
{
    
    [SerializeField] private float speed = 2;
    [SerializeField] private float direction = -1;
    [SerializeField] private bool isLinkedToCamera = false;
    [SerializeField] private GameObject cameras;

    private float startPosX;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        startPosX = transform.position.x;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if ((transform.position.x + spriteRenderer.size.x) < cameras.transform.position.x) {
            Vector3 infiniteScrolling = new Vector3(
                spriteRenderer.size.x * 2,
                0,
                0);
            
            transform.Translate(infiniteScrolling);
        }

        float x = speed * direction;
        
        Vector3 movement = new Vector3(
            x,
            0,
            0
        );

      movement *= Time.deltaTime;
      transform.Translate(movement);

      if(isLinkedToCamera) {

        Vector3 movementY = new Vector3(
            0,
            cameras.transform.position.y - transform.position.y,
            0
        );
        
        movementY *= Time.deltaTime;
        transform.Translate(movementY);
      }

    }
}
