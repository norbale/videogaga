using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
  public GameObject player;
  [SerializeField] private float offsetX = 5;
  [SerializeField] private float offsetY = 2;

  private Vector3 currentOffset;

  void Start()
  {
	Vector3 offset = new Vector3(offsetX, offsetY, 0);
	currentOffset = transform.position - player.transform.position + offset;
  }

  void LateUpdate()
  {
	transform.position = player.transform.position + currentOffset;
  }
}
