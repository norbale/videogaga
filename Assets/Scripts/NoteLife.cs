using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteLife : MonoBehaviour
{
  private bool hit = false;

  private BoxCollider2D boxCollider;
  private void Awake()
  {
	boxCollider = GetComponent<BoxCollider2D>();
  }
  private void OnTriggerEnter2D(Collider2D collision)
  {
	hit = true;
	boxCollider.enabled = false;
	gameObject.SetActive(false);
  }
}
